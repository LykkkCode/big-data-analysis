import pandas as pd
import requests
import json

# 接口为：https://api.apihubs.cn/holiday/get?month=202201&cn=1&size=31
# 关于接口详细信息：https://blog.csdn.net/u012981882/article/details/112552450
if __name__ == '__main__':

    dates = ['201801', '201802', '201803', '201804', '201805', '201806', '201807', '201808', '201809', '201810', '201811'
        , '201812', '201901', '201902', '201903', '201904']

    data = []

    for date in dates:

        print("fetching " + date + " holidays ...")

        url = 'http://api.apihubs.cn/holiday/get?month=' + date + '&cn=1&size=31'
        res = requests.get(url,verify=False)

        # string 转 json
        holiday_json = json.loads(res.text)

        holidays = holiday_json.get('data').get('list')

        for holiday in holidays:
            rows = []
            # 获取日期
            rows.append(holiday.get('date'))
            # 获取星期几
            rows.append(holiday.get('week_cn'))
            # 获取是否为周末 1为周末 2不为周末
            rows.append(holiday.get('weekend'))
            # 获取是否为工作日 1为工作日 2不为工作日
            rows.append(holiday.get('workday'))
            # 获取是否为节假日 1为法定节假日 2不为法定节假日
            rows.append(holiday.get('holiday_legal'))
            # 获取是否为假期节假日 1为假期节假日 2不为假期节假日
            rows.append(holiday.get('holiday_recess'))

            data.append(rows)

    # res = requests.get('http://api.apihubs.cn/holiday/get?month=202201&cn=1&size=31', verify=False)
    # # string 转 json
    # holiday_json = json.loads(res.text)
    #
    # holidays = holiday_json.get('data').get('list')
    # data = []
    # for holiday in holidays:
    #     rows = []
    #     # 获取日期
    #     rows.append(holiday.get('date'))
    #     # 获取星期几
    #     rows.append(holiday.get('week_cn'))
    #     # 获取是否为周末 1为周末 2不为周末
    #     rows.append(holiday.get('weekend'))
    #     # 获取是否为工作日 1为工作日 2不为工作日
    #     rows.append(holiday.get('workday'))
    #     # 获取是否为节假日 1为法定节假日 2不为法定节假日
    #     rows.append(holiday.get('holiday_legal'))
    #     # 获取是否为假期节假日 1为假期节假日 2不为假期节假日
    #     rows.append(holiday.get('holiday_recess'))
    #
    #     data.append(rows)

    columns = ['date', 'week_cn', 'weekend', 'workday', 'holiday_legal', 'holiday_recess']
    df = pd.DataFrame(data,columns=columns)
    df.to_csv('./data_holiday/holiday.csv')
    print('holiday complete!')


    # print(holiday.get('data'))