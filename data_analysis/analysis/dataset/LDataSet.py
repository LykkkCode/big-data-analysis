from torch.utils.data import Dataset

class LDataSet(Dataset):

    def __init__(self, data_info, data_label):
        self.data_info = data_info
        self.data_label = data_label

    def __getitem__(self, index):
        data = self.data_info[index]
        label = self.data_label[index]
        return data, label

    def __len__(self):
        return len(self.data_info)

