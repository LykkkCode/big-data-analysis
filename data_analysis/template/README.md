# kaggle文字数据处理模版

> 以titanic生还预测为例

## 1. 总体预览

```python
train.info()
```

![img](pictures/1226410-20200413135455351-703011139.png)

## 2.  数据初步分析

- 性别与幸存率的关系
- 乘客社会等级与幸存率的关系
- 携带配偶及兄弟姐妹与幸存率的关系
- 携带父母及子女与幸存率的关系
- 年龄与幸存率的关系
- 登港港口与幸存率的关系
- 称呼（从姓名中提取对客户的称呼）与幸存率的关系
- 家庭总人数与幸存率的关系
- 不同船舱的乘客与幸存率的关系

首先我们先看看得到的数据中，幸存的人数与死亡的人数的比重：

```python
res = train['Survived'].value_counts()
print(res)
'''
0    549
1    342
Name: Survived, dtype: int64  
'''
```

### 2.1 性别与生存率的关系

```python
sns.barplot(x='Sex', y='Survived', data=train)
```

![img](pictures/1226410-20200413160051228-381466557.png)

我们可以看到，确实女性幸存率远高于男性，那么性别Sex是一个很重要的特征。

### 2.2 乘客等级与幸存率的关系

```python
sns.barplot(x='Pclass', y='Survived', data=train)
```

![img](pictures/1226410-20200413160946976-1000004248.png)

我们发现，乘客社会等级越高，幸存率越高，所以Pclass这个特征也比较重要。

### 2.3 携带配偶及兄弟姐妹与幸存率的关系

```python
sns.barplot(x='SibSp', y='Survived', data=train)
```

![img](pictures/1226410-20200413162251422-2130722252.png)

只能说：携带的配偶与兄弟姐妹适中的乘客幸存率更高，可能带个配偶幸存率更高。

### 2.4 携带父母及子女与幸存率的关系

```python
sns.barplot(x='Parch', y='Survived', data=train)
```

![img](pictures/1226410-20200413162536685-1829522052.png)

好像也是父母与子女适中的乘客幸存率更高。

### 2.5 年龄与幸存率的关系

```python
facet = sns.FacetGrid(train, hue='Survived', aspect=2)
facet.map(sns.kdeplot, 'Age', shade=True)
facet.set(xlim=(0, train['Age'].max()))
facet.add_legend()
plt.xlabel('Age')
plt.ylabel('density')
```

![img](pictures/1226410-20200413163259434-1407388732.png)

从不同生还情况的密度图中可以看出，在年龄15岁的左侧，生还率有明显的差别，密度图非交叉区域面积非常大，但在其他年龄段，则差别不是很明显，认为是随机所致，因此可以考虑将年龄偏小的区域分离出来。

### 2.6 登港港口与幸存率的关系

登港港口：

- 出发地点：S = 英国南安普顿Southampton
- 途径地点1：C = 法国 瑟堡市Cherbourg
- 途径地点2：Q = 爱尔兰 昆士敦Queenstown

```python
sns.countplot('Embarked',hue='Survived',data=train)
```

![img](pictures/1226410-20200413170206933-1227536681.png)

### 2.7 不同称呼与幸存率的关系

​		每个人都有自己的名字，而且每个名字都是独一无二的，名字和幸存与否看起来并没有直接关联，那怎么利用这个特征呢？有用的信息就隐藏在称呼当中，比如上面我们提到过女生优先，所以称呼为Mrs和 Miss 的就比称呼为Mr 的的更可能幸存，于是我们需要从Name中拿到其称呼并建立新的特征列Title。

　　我们注意在每一个name 中，有一个非常显著的特点：乘客头衔每个名字当中都包含了具体的称谓或者说是头衔，将这部分信息提取出来后可以作为非常有用的一个新变量，可以帮助我们进行预测。

​		从名字中获取头衔：

```python
all_data['Title'] = all_data.Name.apply(lambda name: name.split(',')[1].split('.')[0].strip())
```

​		我们可以看看称呼的种类与数量

```python
all_data.Title.value_counts()

'''
out:
Mr              757
Miss            260
Mrs             197
Master           61
Rev               8
Dr                8
Col               4
Mlle              2
Ms                2
Major             2
Capt              1
Lady              1
Jonkheer          1
Don               1
Dona              1
the Countess      1
Mme               1
Sir               1
Name: Title, dtype: int64
'''
```

我们将定义以下几种头衔类型：

1. Officer政府官员
2. Royalty王室（皇室）
3. Mr已婚男士
4. Mrs已婚妇女
5. Miss年轻未婚女子
6. Master有技能的人/教师

大类可以分为六个：Mr，Miss，Mrs，Master，Royalty，Officer，姓名中头衔字符串与定义头衔类型的分类代码如下：

```python
all_data = pd.concat([train, test], ignore_index=True)
all_data['Title'] = all_data['Name'].apply(lambda x:x.split(',')[1].split('.')[0].strip())
Title_Dict = {}
Title_Dict.update(dict.fromkeys(['Capt', 'Col', 'Major', 'Dr', 'Rev'], 'Officer'))
Title_Dict.update(dict.fromkeys(['Don', 'Sir', 'the Countess', 'Dona', 'Lady'], 'Royalty'))
Title_Dict.update(dict.fromkeys(['Mme', 'Ms', 'Mrs'], 'Mrs'))
Title_Dict.update(dict.fromkeys(['Mlle', 'Miss'], 'Miss'))
Title_Dict.update(dict.fromkeys(['Mr'], 'Mr'))
Title_Dict.update(dict.fromkeys(['Master','Jonkheer'], 'Master'))
 
all_data['Title'] = all_data['Title'].map(Title_Dict)
sns.barplot(x="Title", y="Survived", data=all_data)
```

![img](pictures/1226410-20200413170536674-477376029.png)

### 2.8 家庭总人数与幸存率的关系

​		我们新增FamilyLabel特征，这个特征等于父母儿童+配偶兄弟姐妹+1，在文中就是 FamilyLabel=Parch+SibSp+1，然后将FamilySize分为三类：

```python
all_data['FamilySize']=all_data['SibSp']+all_data['Parch']+1
sns.barplot(x="FamilySize", y="Survived", data=all_data)
```

![img](pictures/1226410-20200413171009155-1572163140.png)

家庭类别：

- 小家庭Family_Single：家庭人数=1
- 中等家庭Family_Small: 2<=家庭人数<=4
- 大家庭Family_Large: 家庭人数>=5

我们按照生存率将FamilySize分为三类，构成FamilyLabel特征：

```python
def Fam_label(s):
    if (s >= 2) & (s <= 4):
        return 2
    elif ((s > 4) & (s <= 7)) | (s == 1):
        return 1
    elif (s > 7):
        return 0
 
all_data['FamilySize'] = all_data['SibSp'] + all_data['Parch'] + 1
all_data['FamilyLabel']=all_data['FamilySize'].apply(Fam_label)
sns.barplot(x="FamilyLabel", y="Survived", data=all_data)
```

![img](pictures/1226410-20200413171348119-1422769873.png)

也可以提取名字长度的特征：

```python
# generating a familysize column  是指所有的家庭成员
all_data['FamilySize'] = all_data['SibSp'] + all_data['Parch']
 
# the .apply method generates a new series
all_data['NameLength'] = all_data['Name'].apply(lambda x: len(x))
```

最后我们选择是否使用这些特征。

### 2.9 不同船舱的乘客与幸存率的关系

​		船舱号（Cabin）里面数据总数是295，缺失了1309-295=1014，缺失率=1014/1309=77.5%，缺失比较大。这注定是个棘手的问题。

​		当然船舱的数据并不是很多，但是乘客位于不同船舱，也就意味着身份不同，所以我们新增Deck特征，先把Cabin空白的填充为“Unknown”，再提取Cabin中的首字母构成乘客的甲板号。

```python
all_data['Cabin'] = all_data['Cabin'].fillna('Unknown')
all_data['Deck'] = all_data['Cabin'].str.get(0)
sns.barplot(x="Deck", y="Survived", data=all_data)
```

![img](pictures/1226410-20200413171654957-1320802751.png)

### 2.10 与二到四人共票号的乘客与幸存率的关系

新增了一个特征叫做 TicketGroup特征，这个特征是统计每个乘客的共票号数。代码如下：

```python
Ticket_Count = dict(all_data['Ticket'].value_counts())
all_data['TicketGroup'] = all_data['Ticket'].apply(lambda x: Ticket_Count[x])
sns.barplot(x='TicketGroup', y='Survived', data=all_data)
```

![img](README.assets/1226410-20200413171932112-2025579711.png)

把生存率按照TicketGroup 分为三类：

```python
def Ticket_Label(s):
    if (s >= 2) & (s <= 4):
        return 2
    elif ((s > 4) & (s <= 8)) | (s == 1):
        return 1
    elif (s > 8):
        return 0
    
Ticket_Count = dict(all_data['Ticket'].value_counts())
all_data['TicketGroup'] = all_data['Ticket'].apply(lambda x: Ticket_Count[x])
all_data['TicketGroup'] = all_data['TicketGroup'].apply(Ticket_Label)
sns.barplot(x='TicketGroup', y='Survived', data=all_data)
```

![img](pictures/1226410-20200413172141694-1353695237.png)

## 3. 数据清洗

### 3.1 缺省值补充

​		上面我们从整体分析的时候，也发现了有着不少的缺失值，缺失量也有比较大的，所以我们如何填充缺失值，这是个问题。

　　很多机器学习算法为了训练模型，要求所传入的特征中不能有空值。一般做如下处理：

1. 如果是数值类型，用平均值取代 .fillna(.mean())
2. 如果是分类数据，用最常见的类别取代 .value_counts() + fillna.()
3. 使用模型预测缺失值，例如KNN

下面可以根据title进行分组并对Age进行补全。

```python
train = pd.read_csv(trainfile)
test = pd.read_csv(testfile)
all_data = pd.concat([train, test], ignore_index=True)
all_data['Title'] = all_data['Name'].apply(lambda x:x.split(',')[1].split('.')[0].strip())
Title_Dict = {}
Title_Dict.update(dict.fromkeys(['Capt', 'Col', 'Major', 'Dr', 'Rev'], 'Officer'))
Title_Dict.update(dict.fromkeys(['Don', 'Sir', 'the Countess', 'Dona', 'Lady'], 'Royalty'))
Title_Dict.update(dict.fromkeys(['Mme', 'Ms', 'Mrs'], 'Mrs'))
Title_Dict.update(dict.fromkeys(['Mlle', 'Miss'], 'Miss'))
Title_Dict.update(dict.fromkeys(['Mr'], 'Mr'))
Title_Dict.update(dict.fromkeys(['Master','Jonkheer'], 'Master'))
 
all_data['Title'] = all_data['Title'].map(Title_Dict)
# sns.barplot(x="Title", y="Survived", data_weather=all_data)
grouped = all_data.groupby(['Title'])
median = grouped.Age.median()
print(median)

'''
Title
Master      4.5
Miss       22.0
Mr         29.0
Mrs        35.0
Officer    49.5
Royalty    40.0
Name: Age, dtype: float64
'''
```

​		可以看到，不同称呼的乘客其年龄的中位数有显著差异，因此我们只需要按称呼对缺失值进行补全即可，这里使用中位数（平均数也是可以的，在这个问题当中两者差异不大，而中位数看起来更整洁一些）。

```python
all_data['Title'] = all_data['Title'].map(Title_Dict)
# sns.barplot(x="Title", y="Survived", data_weather=all_data)
grouped = all_data.groupby(['Title'])
median = grouped.Age.median()
for i in range(len(all_data['Age'])):
    if pd.isnull(all_data['Age'][i]):
        all_data['Age'][i] = median[all_data['Title'][i]]
```

​		Age的缺失值已经补全了，下面我们对Cabin，Embarked 以及Fare进行补全。

​		而Embarked 缺失值只有两个，对结果影响不大，所以我们这里将缺失值补全为登船港口人数最多的港口（这里其实应用的是先验概率最大原则）。从结果来看（上面分析过），S地的登船人数最多，所以我们将缺失值填充为最频繁出现的值，S=英国安南普顿Southampton。

```python
all_data['Embarked'] = all_data['Embarked'].fillna('S')
```

​		上面我们也提到过Cabin缺失数值比较多，所以我们把船舱号（Cabin）的缺失值填充为U，表示未知（Unknown）。

```python
# 缺失数据比较多，船舱号（Cabin）缺失值填充为U，表示未知（Uknow）
all_data['Cabin'] = all_data['Cabin'].fillna('U')
```

​		Fare只有一个缺失值，所以我们可以用票价Fare的平均值补全。

```python
all_data['Fare'] = all_data['Fare'].fillna(all_data.Fare.median())
```

### 3.2 特征处理

​		当补全数据后，我们查看了数据类型，总共有三种数据类型。有整形 int， 有浮点型 float， 有object。我们需要用数值替代类别。有些动作我们前面已经做过了，但是这里总体再过一遍。

- **1.乘客性别（Sex）：男性male，女性 female male=0，female=1**
- **2.登船港口（Embarked）：S，C，Q     S=0， C=1， Q=2**
- **3.乘客姓名（Name）：我们分为六类（具体见前面2.7 不同称呼与幸存率的关系\**）\****

```python
all_data.loc[all_data['Embarked'] == 'S', 'Embarked'] = 0
all_data.loc[all_data['Embarked'] == 'C', 'Embarked'] = 1
all_data.loc[all_data['Embarked'] == 'Q', 'Embarked'] = 2
 
all_data.loc[all_data['Sex'] == 'male', 'Sex'] = 0
all_data.loc[all_data['Sex'] == 'female', 'Sex'] = 1
 
titles = all_data['Name'].apply(get_title)
# print(pd.value_counts(titles))
# map each title to an integer some titles are very rare
# and are compressed into the same codes as other titles
title_mapping = {
    'Mr': 2,
    'Miss': 3,
    'Mrs': 4,
    'Master': 1,
    'Rev': 5,
    'Dr': 5,
    'Col': 5,
    'Mlle': 3,
    'Ms': 4,
    'Major': 6,
    'Don': 5,
    'Countess': 5,
    'Mme': 4,
    'Jonkheer': 1,
    'Sir': 5,
    'Dona': 5,
    'Capt': 6,
    'Lady': 5,
}
for k, v in title_mapping.items():
    titles[titles == k] = v
    # print(k, v)
all_data['Title'] = titles
```

## 4. 特征提取

​		一般来说，一个比较真实的项目，也就是说数据挖掘工程需要建立特征工程，我们需要看看如何提取新的特征，使得我们的准确率更高。这个怎么说呢，其实我们上面对数据分析了那么多，肯定是有原因的。下面一一阐述。

### 4.1 随机森林寻找重要特征

​		上面我们将数据缺失值填充完后，然后进行了概述，总共有14个特征，1个结果（Survived），其中四个特征是object类型，我们去掉，剩下了10个特征，我们做重要特征提取。

![img](pictures/1226410-20200418142048828-778457207.png)

​		我们使用上面五个比较重要的特征：Pclass，Sex，Fare，Title， NameLength 做随机森林模型训练。

```python
def random_forestclassifier_train(traindata, trainlabel, testdata):
    # predictors = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked', 'FamilySize', 'Title', 'NameLength']
    predictors =['Pclass', 'Sex', 'Fare', 'Title', 'NameLength', ]
    traindata, testdata = traindata[predictors], testdata[predictors]
    # print(traindata.shape, trainlabel.shape, testdata.shape)  # (891, 7) (891,) (418, 7)
    # print(testdata.info())
    trainSet, testSet, trainlabel, testlabel = train_test_split(traindata, trainlabel,
                                                                test_size=0.2, random_state=12345)
    # initialize our algorithm class
    clf = RandomForestClassifier(random_state=1, n_estimators=100,
                                 min_samples_split=4, min_samples_leaf=2)
    # training the algorithm using the predictors and target
    clf.fit(trainSet, trainlabel)
    test_accuracy = clf.score(testSet, testlabel) * 100
    print("正确率为   %s%%" % test_accuracy)  # 正确率为   80.44692737430168%
```

### 4.2 相关系数法

相关系数法：计算各个特征的相关系数

```python
# 相关性矩阵
corrDf = all_data.corr()
'''
    查看各个特征与生成情况（Survived）的相关系数，
    ascending=False表示按降序排列
'''
res = corrDf['Survived'].sort_values(ascending=False)
print(res)
```

![img](pictures/1226410-20200418171114273-490657003.png)

我们使用上面六个正相关的特征：Sex，NameLength， Fare，Embarked，Parch， FamiySize， 做随机森林模型训练。

```python
def random_forestclassifier_train(traindata, trainlabel, testdata):
    # predictors = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked', 'FamilySize', 'Title', 'NameLength']
    predictors = ['Sex', 'Fare', 'Embarked', 'NameLength', 'Parch', 'FamilySize']
    traindata, testdata = traindata[predictors], testdata[predictors]
    # print(traindata.shape, trainlabel.shape, testdata.shape)  # (891, 7) (891,) (418, 7)
    # print(testdata.info())
    trainSet, testSet, trainlabel, testlabel = train_test_split(traindata, trainlabel,
                                                                test_size=0.2, random_state=12345)
    # initialize our algorithm class
    clf = RandomForestClassifier(random_state=1, n_estimators=100,
                                 min_samples_split=4, min_samples_leaf=2)
    # training the algorithm using the predictors and target
    clf.fit(trainSet, trainlabel)
    test_accuracy = clf.score(testSet, testlabel) * 100
    print("正确率为   %s%%" % test_accuracy)  # 正确率为   81.56424581005587%
```

### 4.3 尝试用所有特征做随机森林模型训练

```python
def random_forestclassifier_train(traindata, trainlabel, testdata):
    predictors = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked', 'FamilySize', 'Title', 'NameLength']
 
    traindata, testdata = traindata[predictors], testdata[predictors]
    # print(traindata.shape, trainlabel.shape, testdata.shape)  # (891, 7) (891,) (418, 7)
    # print(testdata.info())
    trainSet, testSet, trainlabel, testlabel = train_test_split(traindata, trainlabel,
                                                                test_size=0.2, random_state=12345)
    # initialize our algorithm class
    clf = RandomForestClassifier(random_state=1, n_estimators=100,
                                 min_samples_split=4, min_samples_leaf=2)
    # training the algorithm using the predictors and target
    clf.fit(trainSet, trainlabel)
    test_accuracy = clf.score(testSet, testlabel) * 100
    print("正确率为   %s%%" % test_accuracy)  # 正确率为   83.79888268156425%
```

